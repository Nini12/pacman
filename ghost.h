#ifndef GHOST_H
#define GHOST_H
#include <QGraphicsPixmapItem>
#include <QObject>
#include "case.h"
#include "pacman.h"
#include "map.h"
#include <QVector2D>
#include <QKeyEvent>
#include <QTimer>

class Ghost : public QObject, public QGraphicsPixmapItem{
        Q_OBJECT
public:
    Ghost(QGraphicsItem * parent = NULL);
    Map * map;
    //void setBehaviour();
    //int getBehaviour();
     void keyPressEvent(QKeyEvent * event);
    bool isAgainstWall();
     int direction;
     int getPosxGrille();
     int getPosyGrille();
     void setPosxGrille();
     void setPosyGrille();


public slots:
    void setFocusAuto();
    void momentumG();
    void move();


  //  void changeBehaviour(int anotherBehaviour);


private:
        int posxGrille;
        int posyGrille;
        bool up_momentum = false;
        bool down_momentum = false;
        bool left_momentum = false;
        bool right_momentum = true;
        bool up_momentum_ = false;
        bool down_momentum_ = false;
        bool left_momentum_ = false;
        bool right_momentum_ = false;

};


#endif // GHOST_H
