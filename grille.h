#ifndef GRILLE_H
#define GRILLE_H
#include <QVector>
#include "case.h"
#include "map.h"
#include "pacman.h"
#include <QObject>
#include "ghost.h"


class Grille : public QObject {
    Q_OBJECT
public:
    Grille();
    void drawGrille();
    QVector < QVector < Case * > > getCases();
    void fillCase(int x , int y, int z, QPixmap pix);
    void fillGrille(Map * maptodraw);
    bool canMoveInDirection(int direction, int X , int Y);


    Map * map;
    Pacman * pacman;
    Ghost * ghost;

public slots:
    void ghostMoveTo();
    void pacmanEatPiece();
    void getGhostDirection();




private:
    QVector< QVector < Case* > > cases;

};

#endif // GRILLE_H
