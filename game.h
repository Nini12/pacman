#ifndef GAME_H
#define GAME_H
#include <QWidget>
#include <QGraphicsScene>
#include <QGraphicsView>
#include "grille.h"
#include "pacman.h"
#include "score.h"


class Game : public QGraphicsView{

public:
    Game(QWidget * parent = NULL);
    void start();

    QGraphicsScene * scene;
    Grille * grille;



};


#endif // GAME_H
