#include "game.h"
#include <QGraphicsScene>
#include <QTimer>

Game::Game(QWidget * parent)
{
    //Création de la scene
    scene = new QGraphicsScene;
    scene->setSceneRect(0,0,800,680);

    //On fixe la taille de la view, qui est game
    setScene(scene);
    setFixedSize(800,680);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);



    show();
}

void Game::start()
{


    Grille * grille = new Grille();
    grille->drawGrille();
    grille->fillGrille(grille->map);






}


