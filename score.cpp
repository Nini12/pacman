#include "score.h"
#include <QString>
#include <QFont>
#include "game.h"

extern Game * game;

Score::Score(QGraphicsItem * parent) : QGraphicsTextItem (parent)
{
    score = 0;

    setPlainText(QString("Score : " + QString::number(score)));
    setDefaultTextColor(Qt::white);
    setFont(QFont("monospace",16));
    setPos(600,0);
    setZValue(2);

}

void Score::increase()
{
    score += 10;
    setPlainText(QString("Score : " ) + QString::number(score));
}

int Score::getScore()
{
    return score;
}
