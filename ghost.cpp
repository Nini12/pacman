#include "ghost.h"
#include "game.h"
#include "QtMath"
#include <QDebug>


extern Game * game;

Ghost::Ghost(QGraphicsItem *parent) : QGraphicsPixmapItem (parent)
{
    setPixmap(QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_40.png"));
    setPos(280,280);
    setZValue(2);
    setFlag(QGraphicsItem::ItemIsFocusable);
    //setFocus();
    game->scene->addItem(this);
    direction = 3;
    map = new Map();
    setPosxGrille();
    setPosyGrille();


     QTimer * setfocusTimer = new QTimer;
     connect(setfocusTimer,SIGNAL(timeout()),this, SLOT(setFocusAuto()));
     setfocusTimer->start(1);

    QTimer * timer = new QTimer;
    connect(timer,SIGNAL(timeout()),this,SLOT(momentumG()));
    timer->start(15);

}


void Ghost::setFocusAuto()
{
    this->setFocus();

}

bool Ghost::isAgainstWall()
{
    bool wall;
    int X = getPosxGrille();
    int Y=getPosyGrille();


    if(direction==1){
        if(map->tab[Y][X-1] != 0 and map->tab[Y][X-1] != 13 )
        {

            wall= true;
        }
        else {
            wall=false;
        }
    }
    else if(direction==2){
        if(map->tab[Y][X+1] != 0 and map->tab[Y][X+1] != 13 )
        {

            wall= true;
        }
        else {
            wall=false;
        }
    }
    else if(direction==3){
        if(map->tab[Y-1][X] != 0 and map->tab[Y-1][X] != 13)
        {

            wall= true;
        }
        else {
            wall=false;
        }

    }
    else if(direction==4){
        if(map->tab[Y+1][X] != 0 and map->tab[Y+1][X] != 13)
        {

            wall= true;

        }
        else {
            wall=false;
        }


    }

    return wall;
}

void Ghost::keyPressEvent(QKeyEvent * event)
{
    this->setFocus();

    int X=getPosxGrille();
    int Y=getPosyGrille();
    int x = int(pos().x());
    int y = int(pos().y());

    if(event->key() == Qt::Key_Q){
        if(map->tab[Y][X-1] ==0 or map->tab[Y][X-1] ==13 or map->tab[Y+1][X-1] ==0 or map->tab[Y+1][X-1] ==13 or map->tab[Y-1][X-1] ==0 or map->tab[Y-1][X-1] ==13){

            up_momentum_ = false;
            down_momentum_ = false;
            left_momentum_ = true;
            right_momentum_ = false;


        //setPixmap(QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_55.png"));

        //setPos(x()-20,y());
            }

        }
    else if(event->key() == Qt::Key_D){
        if(map->tab[Y][X+1] ==0 or map->tab[Y][X+1] ==13 or map->tab[Y+1][X+1] ==0 or map->tab[Y+1][X+1] ==13 or map->tab[Y-1][X+1] ==0 or map->tab[Y-1][X+1] ==13){
        //setPixmap(QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_75.png"));
        up_momentum_ = false;
        down_momentum_ = false;
        left_momentum_ = false;
        right_momentum_ = true;


        //setPos(x()+20,y());
        }
    }
    else if(event->key() == Qt::Key_Z){
        if(map->tab[Y-1][X] ==0 or map->tab[Y-1][X] ==13 or map->tab[Y-1][X-1] ==0 or map->tab[Y-1][X-1] ==13 or map->tab[Y-1][X+1] ==0 or map->tab[Y-1][X+1] ==13){
        //setPixmap(QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_102.png"));
        up_momentum_ = true;
        down_momentum_ = false;
        left_momentum_ = false;
        right_momentum_ = false;


        //setPos(x(),y()-20);
        }
    }
    else if(event->key() == Qt::Key_S){
        if(map->tab[Y+1][X] == 0 or map->tab[Y+1][X] == 13 or map->tab[Y+1][X+1] == 0 or map->tab[Y+1][X+1] == 13 or map->tab[Y+1][X-1] == 0 or map->tab[Y+1][X-1] == 13)
        {

        up_momentum_ = false;
        down_momentum_ = true;
        left_momentum_ = false;
        right_momentum_ = false;



        }
    }
}


int Ghost::getPosxGrille()
{
    //qDebug() << "x:"<<posxGrille;
    return posxGrille;
}

int Ghost::getPosyGrille()
{
    //qDebug() << "y:"<<posyGrille;
    return posyGrille;
}

void Ghost::setPosxGrille()
{
    posxGrille = qFloor(pos().x()/20);

}

void Ghost::setPosyGrille()
{
    posyGrille = qFloor(pos().y()/20);

}

void Ghost::move()
{
    int step = 2;
    int X=getPosxGrille();
    int Y=getPosyGrille();
    int x = int(pos().x());
    int y = int(pos().y());

    if(direction == 1){
        if((map->tab[Y][X-1] ==0 or map->tab[Y][X-1] ==13) ){
        setPixmap(QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_38.png"));
        setPos(x-step,y);
           }
        }
    else if(direction == 2){
        if((map->tab[Y][X+1] ==0 or map->tab[Y][X+1] ==13) ){
        setPixmap(QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_39.png"));
        setPos(x+step,y);
       }
    }
    else if(direction == 3){
        if((map->tab[Y-1][X] ==0 or map->tab[Y-1][X] ==13)){
        setPixmap(QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_41.png"));
        setPos(x,y-step);
        }
    }
    else if(direction == 4 ){
        if((map->tab[Y+1][X] ==0 or map->tab[Y+1][X] ==13) ){
        setPixmap(QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_40.png"));
        setPos(x,y+step);
        }
    }

    setPosxGrille();
    setPosyGrille();
}



void Ghost::momentumG()
{
    int step = 2;
    int X=getPosxGrille();
    int Y=getPosyGrille();
    int x = int(pos().x());
    int y = int(pos().y());
    qDebug()<<X<<x;
    if (y%20 == 0 and x%20 == 0 and left_momentum_ == true)
    {qDebug()<<"here";
        if (map->tab[Y][X-1] == 0 or map->tab[Y][X-1] == 13)
        {
            up_momentum = up_momentum_;
            down_momentum = down_momentum_;
            left_momentum = left_momentum_;
            right_momentum = right_momentum_;
        }
    }

    if (y%20 == 0 and x%20 == 0 and right_momentum_ == true)
    {
        if (map->tab[Y][X+1] == 0 or map->tab[Y][X+1] == 13)
        {
            up_momentum = up_momentum_;
            down_momentum = down_momentum_;
            left_momentum = left_momentum_;
            right_momentum = right_momentum_;
        }
    }

    if (x%20 == 0 and x%20 == 0 and up_momentum_ == true)
    {
        if (map->tab[Y-1][X] == 0 or map->tab[Y-1][X] == 13)
        {
            up_momentum = up_momentum_;
            down_momentum = down_momentum_;
            left_momentum = left_momentum_;
            right_momentum = right_momentum_;
        }
    }

    if (x%20 == 0 and x%20 == 0 and down_momentum_ == true)
    {
        if (map->tab[Y+1][X] == 0 or map->tab[Y+1][X] == 13)
        {
            up_momentum = up_momentum_;
            down_momentum = down_momentum_;
            left_momentum = left_momentum_;
            right_momentum = right_momentum_;
        }
    }


    if(left_momentum == true){
        if((map->tab[Y][X-1] ==0 or map->tab[Y][X-1] ==13 or (X*20 - x) != 0) and y%20 == 0){
        setPixmap(QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_38.png"));
        setPos(x-step,y);
            }
        }
    else if(right_momentum == true){
        if((map->tab[Y][X+1] ==0 or map->tab[Y][X+1] ==13) and y%20 == 0 ){
        setPixmap(QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_39.png"));
        setPos(x+step,y);
        }
    }
    else if(up_momentum == true){
        if((map->tab[Y-1][X] ==0 or map->tab[Y-1][X] ==13 or (Y*20 - y) != 0) and x%20 == 0 ){
        setPixmap(QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_41.png"));
        setPos(x,y-step);
        }
    }
    else if(down_momentum == true and x%20 == 0){
        if(map->tab[Y+1][X] == 0 or map->tab[Y+1][X] == 13){
        setPixmap(QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_40.png"));
        setPos(x,y+step);
        }
    }

    setPosxGrille();
    setPosyGrille();
}




