#include "pacman.h"
#include <QKeyEvent>
#include "game.h"
#include <QDebug>

#include <QtMath>
#include <QMediaPlayer>

extern Game * game;

Pacman::Pacman(QGraphicsItem * parent) : QGraphicsPixmapItem(parent)
{

    setPixmap(QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_89.png"));
    setPos(20,20);
    setZValue(2);
    game->scene->addItem(this);
    setFlag(QGraphicsItem::ItemIsFocusable);
    //setFocus();
    map =  new Map;
    score= new Score();
    game->scene->addItem(score);

    QTimer * setfocusTimer = new QTimer;
    connect(setfocusTimer,SIGNAL(timeout()),this, SLOT(setFocusAuto()));
    setfocusTimer->start(2);

   QTimer * timer = new QTimer;
   connect(timer,SIGNAL(timeout()),this,SLOT(momentumP()));
   timer->start(15);
}



void Pacman::setFocusAuto()
{
    this->setFocus();


}



void Pacman::keyPressEvent(QKeyEvent *event)
{
    setFocus();

    int X=getPosxGrille();
    int Y=getPosyGrille();
    int x = int(pos().x());
    int y = int(pos().y());

    if(event->key() == Qt::Key_Left){
        if(this->map->tab[Y][X-1] ==0 or map->tab[Y][X-1] ==13 or map->tab[Y+1][X-1] ==0 or map->tab[Y+1][X-1] ==13 or map->tab[Y-1][X-1] ==0 or map->tab[Y-1][X-1] ==13){
        //setPixmap(QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_55.png"));
        up_momentum_ = false;
        down_momentum_ = false;
        left_momentum_ = true;
        right_momentum_ = false;

        //setPos(x()-20,y());
            }

        }
    else if(event->key() == Qt::Key_Right){
        if(map->tab[Y][X+1] ==0 or map->tab[Y][X+1] ==13 or map->tab[Y+1][X+1] ==0 or map->tab[Y+1][X+1] ==13 or map->tab[Y-1][X+1] ==0 or map->tab[Y-1][X+1] ==13){
        //setPixmap(QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_75.png"));
        up_momentum_ = false;
        down_momentum_ = false;
        left_momentum_ = false;
        right_momentum_ = true;

        //setPos(x()+20,y());
        }
    }
    else if(event->key() == Qt::Key_Up){
        if(map->tab[Y-1][X] ==0 or map->tab[Y-1][X] ==13 or map->tab[Y-1][X-1] ==0 or map->tab[Y-1][X-1] ==13 or map->tab[Y-1][X+1] ==0 or map->tab[Y-1][X+1] ==13){
        //setPixmap(QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_102.png"));
        up_momentum_ = true;
        down_momentum_ = false;
        left_momentum_ = false;
        right_momentum_ = false;

        //setPos(x(),y()-20);
        }
    }
    else if(event->key() == Qt::Key_Down){
        if(map->tab[Y+1][X] == 0 or map->tab[Y+1][X] == 13 or map->tab[Y+1][X+1] == 0 or map->tab[Y+1][X+1] == 13 or map->tab[Y+1][X-1] == 0 or map->tab[Y+1][X-1] == 13)
        {

        up_momentum_ = false;
        down_momentum_ = true;
        left_momentum_ = false;
        right_momentum_ = false;

        }
    }
}

void Pacman::increaseNbPiece()
{
    nbPiece = nbPiece + 1;
    score->increase();
    //qDebug() << "piece" << nbPiece;

}


void Pacman::increaseCnt()
{
    cnt = cnt+50;
    //qDebug() << "increaseCnt" << cnt << "fdp";
    if (cnt==600)
    {
        cnt=0;
    }
}


QPixmap Pacman::pictChangeRight()
{

    if (cnt>=0 and cnt<100)
    {
        increaseCnt();
        return QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_75.png");
    }
    else if (cnt>=100 and cnt<200)
    {
        increaseCnt();
        return QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_74.png");
    }
    else if (cnt>=200 and cnt<300)
    {
        increaseCnt();
        return QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_73.png");
    }
    else if (cnt>=300 and cnt<400)
    {
        increaseCnt();
        return QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_72.png");
    }
    else if (cnt>=400 and cnt<500)
    {
        increaseCnt();
        return QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_73.png");
    }
    else if (cnt>=500 and cnt<600)
    {
        increaseCnt();
        return QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_74.png");
    }

}

QPixmap Pacman::pictChangeLeft()
{
    if (cnt>=0 and cnt<100)
    {
        increaseCnt();
        return QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_55.png");
    }
    else if (cnt>=100 and cnt<200)
    {
        increaseCnt();
        return QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_56.png");
    }
    else if (cnt>=200 and cnt<300)
    {
        increaseCnt();
        return QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_57.png");
    }
    else if (cnt>=300 and cnt<400)
    {
        increaseCnt();
        return QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_58.png");
    }
    else if (cnt>=400 and cnt<500)
    {
        increaseCnt();
        return QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_57.png");
    }
    else if (cnt>=500 and cnt<600)
    {
        increaseCnt();
        return QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_56.png");
    }

}

QPixmap Pacman::pictChangeUpDown()
{
    if (cnt>=0 and cnt<100)
    {
        increaseCnt();
        return QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_89.png");
    }
    else if (cnt>=100 and cnt<200)
    {
        increaseCnt();
        return QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_90.png");
    }
    else if (cnt>=200 and cnt<300)
    {
        increaseCnt();
        return QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_91.png");
    }
    else if (cnt>=300 and cnt<400)
    {
        increaseCnt();
        return QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_92.png");
    }
    else if (cnt>=400 and cnt<500)
    {
        increaseCnt();
        return QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_91.png");
    }
    else if (cnt>=500 and cnt<600)
    {
        increaseCnt();
        return QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_90.png");
    }
}

int Pacman::getPosxGrille()
{
    return posxGrille;
}

int Pacman::getPosyGrille()
{
    return posyGrille;
}

void Pacman::setPosxGrille()
{
    posxGrille = qFloor(pos().x()/20);

}

void Pacman::setPosyGrille()
{
    posyGrille = qFloor(pos().y()/20);

}


void Pacman::momentumP()
{
    int step = 2;
    int X=getPosxGrille();
    int Y=getPosyGrille();
    int x = int(pos().x());
    int y = int(pos().y());

    //canaux de téléportation
    if(X== 0 and Y==16)
    {
        QMediaPlayer * music = new QMediaPlayer();
        music->setMedia(QUrl("qrc:/sounds/Ressources/sounds/waka.wav"));
        music->play();
        setPos(540,320);
    }

    if(X== 27 and Y==16)
    {
        QMediaPlayer * music = new QMediaPlayer();
        music->setMedia(QUrl("qrc:/sounds/Ressources/sounds/waka.wav"));
        music->play();
        setPos(0,320);
    }

    //
    if (y%20 == 0 and x%20 == 0 and left_momentum_ == true)
    {
        if (map->tab[Y][X-1] == 0 or map->tab[Y][X-1] == 13)
        {
            up_momentum = up_momentum_;
            down_momentum = down_momentum_;
            left_momentum = left_momentum_;
            right_momentum = right_momentum_;
        }
    }

    if (y%20 == 0 and x%20 == 0 and right_momentum_ == true)
    {
        if (map->tab[Y][X+1] == 0 or map->tab[Y][X+1] == 13)
        {
            up_momentum = up_momentum_;
            down_momentum = down_momentum_;
            left_momentum = left_momentum_;
            right_momentum = right_momentum_;
        }
    }

    if (x%20 == 0 and x%20 == 0 and up_momentum_ == true)
    {
        if (map->tab[Y-1][X] == 0 or map->tab[Y-1][X] == 13)
        {
            up_momentum = up_momentum_;
            down_momentum = down_momentum_;
            left_momentum = left_momentum_;
            right_momentum = right_momentum_;
        }
    }

    if (x%20 == 0 and x%20 == 0 and down_momentum_ == true)
    {
        if (map->tab[Y+1][X] == 0 or map->tab[Y+1][X] == 13)
        {
            up_momentum = up_momentum_;
            down_momentum = down_momentum_;
            left_momentum = left_momentum_;
            right_momentum = right_momentum_;
        }
    }


    if(left_momentum == true){
        if((map->tab[Y][X-1] ==0 or map->tab[Y][X-1] ==13 or (X*20 - x) != 0) and y%20 == 0){
        setPixmap(pictChangeLeft());
        setPos(x-step,y);
            }
        }
    else if(right_momentum == true){
        if((map->tab[Y][X+1] ==0 or map->tab[Y][X+1] ==13) and y%20 == 0 ){
        setPixmap(pictChangeRight());
        setPos(x+step,y);
        }
    }
    else if(up_momentum == true){
        if((map->tab[Y-1][X] ==0 or map->tab[Y-1][X] ==13 or (Y*20 - y) != 0) and x%20 == 0 ){
        setPixmap(pictChangeUpDown());
        setPos(x,y-step);
        }
    }
    else if(down_momentum == true and x%20 == 0){
        if(map->tab[Y+1][X] == 0 or map->tab[Y+1][X] == 13){
        setPixmap(pictChangeUpDown());
        setPos(x,y+step);
        }
    }

    setPosxGrille();
    setPosyGrille();
}



