#ifndef PACMAN_H
#define PACMAN_H
#include <QGraphicsPixmapItem>
#include <QPixmap>
#include <QObject>
#include "case.h"
#include "map.h"
#include "score.h"
#include <QTimer>

class Pacman : public QObject, public QGraphicsPixmapItem {
        Q_OBJECT
public :
    Pacman(QGraphicsItem * parent = NULL);
    void keyPressEvent(QKeyEvent * event);
    void increaseNbPiece();
    void increaseCnt();
    QPixmap pictChangeRight();
    QPixmap pictChangeLeft();
    QPixmap pictChangeUpDown();
    Map * map;
    Score * score;
    int getPosxGrille();
    int getPosyGrille();
    void setPosxGrille();
    void setPosyGrille();

public slots:
    void setFocusAuto();
    void momentumP();


private:
    int posxGrille;
    int posyGrille;
    int nbPiece = 0;
    int cnt=0;
    bool up_momentum = false;
    bool down_momentum = false;
    bool left_momentum = false;
    bool right_momentum = true;
    bool up_momentum_ = false;
    bool down_momentum_ = false;
    bool left_momentum_ = false;
    bool right_momentum_ = false;

};

#endif // PACMAN_H
