#include "grille.h"
#include "game.h"
#include <QBrush>
#include "QDebug"
#include <QPixmap>
#include <QGraphicsPixmapItem>
#include <QTimer>
#include <QObject>
#include <QMediaPlayer>


extern Game * game;

Grille::Grille()
{
    map = new Map();
    pacman = new Pacman();
    ghost = new Ghost();

    QTimer * timer = new QTimer;
    connect(timer,SIGNAL(timeout()),this,SLOT(pacmanEatPiece()));
    //connect(timer,SIGNAL(timeout()),this,SLOT(ghostMoveTo()));
    timer->start(15);

    QTimer * timer2 = new QTimer;
    //connect(timer2,SIGNAL(timeout()),this,SLOT(getGhostDirection()));
    timer2->start(80);




}

void Grille::drawGrille()
{
    for (int i = 0; i<80;i++) {
        //On rajoute la ligne de cases
        cases.append(QVector<Case *>());
        for (int j=0; j<60;j++) {
            //On ajoute chaque cases
            Case * unecase = new Case();
            unecase->setPos(i*20,j*20);
            cases[i].append(unecase);
            game->scene->addItem(unecase);
        }
    }
}

QVector<QVector<Case *>> Grille::getCases()
{
    return cases;
}

void Grille::fillCase(int x, int y, int z, QPixmap pix)
{
    cases[y][x]->pixmap = pix;
    QGraphicsPixmapItem *item = new QGraphicsPixmapItem(cases[y][x]->pixmap);
    item->setPixmap(cases[y][x]->pixmap);
    item->setPos(cases[y][x]->pos().x(),cases[y][x]->pos().y());
    item->setZValue(z);
    game->scene->addItem(item);
}

void Grille::fillGrille(Map * maptodraw)
{
    for (int j=0; j < cases.size();j++) {
         for (int i=0;i<cases[0].size();i++) {
            fillCase(i,j,1,QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_238.png"));
        }
    }
    for (int j=0;j<28;j++) {
         for (int i =0; i<34;i++) {
             if(maptodraw->tab[i][j] == 0){
                 fillCase(i,j,1,QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_238.png"));
             }
             else if(maptodraw->tab[i][j] == 1) {
                 fillCase(i,j,1,QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_176.png"));
             }

             else if(maptodraw->tab[i][j] == 2) {
                 fillCase(i,j,1,QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_178.png"));
             }

             else if(maptodraw->tab[i][j] == 3) {
                 fillCase(i,j,1,QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_210.png"));
             }

             else if(maptodraw->tab[i][j] == 4) {
                 fillCase(i,j,1,QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_212.png"));
             }

             else if(maptodraw->tab[i][j] == 5) {
                 fillCase(i,j,1,QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_227.png"));
             }

             else if(maptodraw->tab[i][j] == 6) {
                 fillCase(i,j,1,QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_228.png"));
             }

             else if(maptodraw->tab[i][j] == 7) {
                 fillCase(i,j,1,QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_177.png"));
             }

             else if(maptodraw->tab[i][j] == 8) {
                 fillCase(i,j,1,QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_211.png"));
             }

             else if(maptodraw->tab[i][j] == 9) {
                 fillCase(i,j,1,QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_230.png"));
             }

             else if(maptodraw->tab[i][j] == 10) {
                 fillCase(i,j,1,QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_211.png"));
             }

             else if(maptodraw->tab[i][j] == 11) {
                 fillCase(i,j,1,QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_213.png"));
             }

             else if(maptodraw->tab[i][j] == 12) {
                 fillCase(i,j,1,QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_231.png"));
             }

             else if(maptodraw->tab[i][j] == 13) {
                 fillCase(i,j,1,QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_8.png"));
             }

             else if(maptodraw->tab[i][j] == 14) {
                 fillCase(i,j,1,QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_214.png"));
             }

             else if(maptodraw->tab[i][j] == 15) {
                 fillCase(i,j,1,QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_243.png"));
             }
        }
    }
}



void Grille::pacmanEatPiece()
{
    if(this->map->tab[pacman->getPosyGrille()][pacman->getPosxGrille()] == 13){
        this->map->tab[pacman->getPosyGrille()][pacman->getPosxGrille()] = 0;
        fillCase(pacman->getPosyGrille(),pacman->getPosxGrille(),1,QPixmap(":/new/prefix1/Ressources/spr_all5_4A2_separate images/rosekane_238.png"));
        pacman->increaseNbPiece();
        QMediaPlayer * music = new QMediaPlayer();
        music->setMedia(QUrl("qrc:/sounds/Ressources/sounds/waka.wav"));
        music->play();
    }


}

void Grille::getGhostDirection()
{


    if(ghost->isAgainstWall()){

        int Xg=ghost->getPosxGrille();
        int Yg=ghost->getPosyGrille();
        int Xp=pacman->getPosxGrille();
        int Yp=pacman->getPosyGrille();


    if( Xg <= Xp and Yg <= Yp ){// pacman est a droite et en bas du ghost
        if(canMoveInDirection(2,Xg,Yg)){
                ghost->direction=2;//droite
         }
        else if(canMoveInDirection(4,Xg,Yg)) {
                ghost->direction=4;//bas
        }
        else if(canMoveInDirection(3,Xg,Yg)) {
            ghost->direction=3;//haut
        }
        else if(canMoveInDirection(1,Xg,Yg)) {
            ghost->direction=1;//gauche
        }

    }
    else if (Xg <= Xp and Yg >= Yp){//pacman est a droite et en haut du ghost
        if(canMoveInDirection(2,Xg,Yg)){
                ghost->direction=2;//droite
         }
        else if(canMoveInDirection(3,Xg,Yg)) {
                ghost->direction=3;//haut
        }
        else if(canMoveInDirection(4,Xg,Yg)) {
            ghost->direction=4;//bas
        }
        else if(canMoveInDirection(1,Xg,Yg)) {
            ghost->direction=1;//gauche
        }

    }
    else if( Xg >= Xp and Yg >= Yp){//pacman est a gauche et en haut du ghost
        if(canMoveInDirection(1,Xg,Yg)){

                ghost->direction=1;//gauche
         }
        else if(canMoveInDirection(3,Xg,Yg)) {
                ghost->direction=3;//haut
        }
        else if(canMoveInDirection(4,Xg,Yg)) {
            ghost->direction=4;//bas
        }
        else if(canMoveInDirection(2,Xg,Yg)) {
            ghost->direction=2;//droite
        }

    }
    else if(Xg >= Xp and Yg <= Yp){//pacmane est a gauche et en bas du ghost
        if(canMoveInDirection(1,Xg,Yg)){
                ghost->direction=1;//gauche
         }
        else if(canMoveInDirection(4,Xg,Yg)) {
                ghost->direction=4;//bas
        }
        else if(canMoveInDirection(3,Xg,Yg)) {
            ghost->direction=3;//haut
        }
        else if(canMoveInDirection(2,Xg,Yg)) {
            ghost->direction=2;//droite
        }

    }
    }
}


bool Grille::canMoveInDirection(int direction , int X , int Y)
{

    bool canmove;

    if(direction==1){//direction gauche
        if(map->tab[Y][X-1] == 0 or map->tab[Y][X-1] == 13 ){
            canmove= true;
        }
        else{
            canmove=false;
        }

    }
    else if (direction == 2){//direction droite
        if(map->tab[Y][X+1] == 0 or map->tab[Y][X+1] == 13 ){
            canmove= true;
        }
        else{
            canmove= false;
        }
    }
    else if (direction == 3){//direction haut
        if(map->tab[Y-1][X] == 0 or map->tab[Y-1][X] == 13 ){
            canmove= true;
        }
        else{
            canmove= false;
        }
    }
    else{//direction bas
        if(map->tab[Y+1][X] == 0 or map->tab[Y+1][X] == 13 ){
            canmove= true;
        }
        else{
            canmove= false;
        }
    }

    return canmove;
}

void Grille::ghostMoveTo()
{

   this->ghost->move();
}
